CREATE TABLE IF NOT EXISTS staffs (
    `id`            BIGINT              AUTO_INCREMENT PRIMARY KEY,
    `first_name`    VARCHAR(64)         NOT NULL,
    `last_name`     VARCHAR(64)         NOT NULL
)
