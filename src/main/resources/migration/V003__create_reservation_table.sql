CREATE TABLE IF NOT EXISTS reservations (
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY,
    `user_name` VARCHAR(128) NOT NULL,
    `company_name` VARCHAR(64) NOT NULL,
    `zone_id` VARCHAR(64) NOT NULL,
    `start_time` VARCHAR(64) NOT NULL,
    `duration` VARCHAR(64) NOT NULL,
    `staff_id` BIGINT,
    FOREIGN KEY (`staff_id`) REFERENCES staffs(`id`)
)