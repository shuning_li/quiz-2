CREATE TABLE IF NOT EXISTS timezones (
    `id`       BIGINT AUTO_INCREMENT PRIMARY KEY,
    `zone_id`   VARCHAR(64) NOT NULL,
    `staff_id` BIGINT,
    FOREIGN KEY (`staff_id`) REFERENCES staffs (`id`)
)