package com.twuc.webApp.controller;


import com.twuc.webApp.contact.CreateReservationRequest;
import com.twuc.webApp.contact.CreateStaffRequest;
import com.twuc.webApp.contact.GetStaffResponse;
import com.twuc.webApp.contact.UpdateTimezoneRequest;
import com.twuc.webApp.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Duration;
import java.time.OffsetDateTime;

@RestController
@RequestMapping("/api/staffs")
public class StaffController {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private TimezoneRepository timezoneRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    @PostMapping("")
    public ResponseEntity<Object> createStaff(@Valid @RequestBody CreateStaffRequest createStaffRequest) {
        String firstName = createStaffRequest.getFirstName();
        String lastName = createStaffRequest.getLastName();
        Staff staff = new Staff(firstName, lastName);
        staffRepository.saveAndFlush(staff);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", "/api/staffs/" + staff.getId())
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetStaffResponse> getStaff(@PathVariable Long id) {
        Staff staff = staffRepository.getOne(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new GetStaffResponse(staff));
    }

    @PutMapping("/{id}/timezone")
    @ResponseStatus(HttpStatus.OK)
    public void updateTimezone(@PathVariable Long id, @Valid @RequestBody UpdateTimezoneRequest updateTimezoneRequest) {
        Staff staff = staffRepository.getOne(id);
        Timezone timezone = new Timezone(updateTimezoneRequest.getZoneId());
        staff.setTimezone(timezone);
        timezoneRepository.saveAndFlush(timezone);
    }

    @PostMapping("/{id}/reservations")
    public ResponseEntity<Object> createReservation(@PathVariable Long id, @Valid @RequestBody CreateReservationRequest createReservationRequest) {
        String userName = createReservationRequest.getUserName();
        String companyName = createReservationRequest.getCompanyName();
        OffsetDateTime startTime = createReservationRequest.getStartTime();
        Duration duration = createReservationRequest.getDuration();
        String zoneId = createReservationRequest.getZoneId();

        Staff staff = staffRepository.getOne(id);

        Reservation reservation = new Reservation(userName, companyName, zoneId, startTime, duration);

        staff.addReservation(reservation);

        reservationRepository.save(reservation);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .header("Location", String.format("/api/staffs/%d/reservations", staff.getId()))
                .build();
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {}
}
