package com.twuc.webApp.contact;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.Timezone;

import java.util.Objects;

public class GetStaffResponse {
    private String firstName;
    private String lastName;
    private String zoneId;

    public GetStaffResponse(Staff staff, Timezone timezone) {
        Objects.requireNonNull(staff);

        this.firstName = staff.getFirstName();
        this.lastName = staff.getLastName();
        this.zoneId = timezone.getZoneId();
    }

    public GetStaffResponse(Staff staff) {
        Objects.requireNonNull(staff);

        this.firstName = staff.getFirstName();
        this.lastName = staff.getLastName();
        this.zoneId = Objects.isNull(staff.getTimezone()) ? null : staff.getTimezone().getZoneId();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZoneId() {
        return zoneId;
    }
}
