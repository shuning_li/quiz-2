package com.twuc.webApp.contact;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateStaffRequest {
    @NotNull
    @Size(min = 1, max = 64)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 64)
    private String lastName;

    public CreateStaffRequest() {
    }

    public CreateStaffRequest(
            @NotNull @Size(min = 1, max = 64) String firstName,
            @NotNull @Size(min = 1, max = 64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
