package com.twuc.webApp.contact;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UpdateTimezoneRequest {
    @NotNull
    @Pattern(regexp = "(^[A-Z]\\w+)\\/([A-Z]\\w+)")
    private String zoneId;

    public UpdateTimezoneRequest() {
    }

    public UpdateTimezoneRequest(
            @NotNull @Pattern(regexp = "(^[A-Z]\\w+)\\/([A-Z]\\w+)")  String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }
}
