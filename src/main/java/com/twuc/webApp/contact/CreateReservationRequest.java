package com.twuc.webApp.contact;

import java.time.Duration;
import java.time.OffsetDateTime;

public class CreateReservationRequest {
    private String userName;
    private String companyName;
    private String zoneId;
    private OffsetDateTime startTime;
    private Duration duration;

    public CreateReservationRequest() {
    }

    public CreateReservationRequest(String userName, String companyName, String zoneId, OffsetDateTime startTime, Duration duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public OffsetDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }
}
