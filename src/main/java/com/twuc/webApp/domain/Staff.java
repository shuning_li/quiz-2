package com.twuc.webApp.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Staff {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 64)
    private String firstName;
    @Column(nullable = false, length = 64)
    private String lastName;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "staff")
    private Timezone timezone;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "staff")
    private List<Reservation> reservations;

    public Staff() {
    }

    public Staff(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Timezone getTimezone() {
        return timezone;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }
}
