package com.twuc.webApp.domain;

import javax.persistence.*;
import java.time.Duration;
import java.time.OffsetDateTime;

@Entity
public class Reservation {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 128)
    private String userName;
    @Column(nullable = false, length = 64)
    private String companyName;
    @Column(nullable = false, length = 64)
    private String zoneId;
    @Column(nullable = false, length = 64)
    private OffsetDateTime startTime;
    @Column(nullable = false, length = 64)
    private Duration duration;
    @ManyToOne
    private Staff staff;

    public Reservation() {
    }

    public Reservation(String userName, String companyName, String zoneId, OffsetDateTime startTime, Duration duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public OffsetDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }
}
