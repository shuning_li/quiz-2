package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Timezone {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String zoneId;
    @OneToOne
    private Staff staff;

    public Timezone() {
    }

    public Timezone(String zoneId) {
        this.zoneId = zoneId;
    }

    public Long getId() {
        return id;
    }

    public String getZoneId() {
        return zoneId;
    }
}
