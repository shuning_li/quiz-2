package com.twuc.webApp;

import com.twuc.webApp.contact.CreateReservationRequest;
import com.twuc.webApp.contact.GetStaffResponse;
import com.twuc.webApp.contact.UpdateTimezoneRequest;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.Timezone;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.zone.ZoneRulesProvider;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StaffControllerTest extends ApiTestBase {
    @Test
    void should_create_and_save_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("Li", "Shuning"))))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_get_existed_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("Li", "Shuning"))));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(serialize(new GetStaffResponse(new Staff("Li", "Shuning")))));
    }

    @Test
    void should_return_400_when_params_is_invalid() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff(null, null))))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_staff_which_contains_zoneId() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("Li", "Shuning"))));

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new UpdateTimezoneRequest("Asia/Chongqing"))));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(serialize(new GetStaffResponse(new Staff("Li", "Shuning"), new Timezone("Asia/Chongqing")))));
    }

    @Test
    void should_return_400_when_zoneId_is_not_correct_or_empty() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("Li", "Shuning"))));

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new UpdateTimezoneRequest("asia/chongqing"))))
                .andExpect(status().isBadRequest());

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new UpdateTimezoneRequest(""))))
                .andExpect(status().isBadRequest());
    }

    @Disabled
    @Test
    void should_create_and_save_reservation() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff("Li", "Shuning"))));

        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(
                        new CreateReservationRequest(
                                "xiaoming", "TW", "Asia/Shenzhen",
                                OffsetDateTime.now(), Duration.ofHours(1))))
        ).andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/staffs/1/reservations"));
    }
}
