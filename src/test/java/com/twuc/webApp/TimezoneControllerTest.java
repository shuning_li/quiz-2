package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.zone.ZoneRulesProvider;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class TimezoneControllerTest extends ApiTestBase {
    @Test
    void should_get_available_timezones() throws Exception {
        Set<String> expected = ZoneRulesProvider.getAvailableZoneIds();

        mockMvc.perform(get("/api/timezones"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(serialize(expected)));
    }
}
