package com.twuc.webApp;

import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.StaffRepository;
import com.twuc.webApp.domain.Timezone;
import com.twuc.webApp.domain.TimezoneRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class StaffRepositoryTest {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private TimezoneRepository timezoneRepository;

    @Test
    void should_create_and_save_staff() {
        Staff staff = new Staff("Li", "Shuning");

        staffRepository.saveAndFlush(staff);

        Staff savedStaff = staffRepository.getOne(staff.getId());

        assertNotNull(savedStaff);
    }

    @Test
    void should_save_timezone_when_save_staff() {
        Staff staff = new Staff("Li", "Shuning");
        Timezone timezone = new Timezone("Asia/Chongqing");
        staff.setTimezone(timezone);

        staffRepository.saveAndFlush(staff);

        Staff savedStaff = staffRepository.getOne(staff.getId());

        assertNotNull(savedStaff.getTimezone());
    }
}
