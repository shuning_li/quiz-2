package com.twuc.webApp;

import com.twuc.webApp.contact.CreateReservationRequest;
import com.twuc.webApp.domain.Reservation;
import com.twuc.webApp.domain.ReservationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import java.time.Duration;
import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ReservationRepositoryTest {
    @Autowired
    private ReservationRepository reservationRepository;

    @Test
    void should_create_and_save_reservation() {
        Reservation reservation = new Reservation(
                "xiaoming", "TW", "Asia/Shenzhen",
                OffsetDateTime.now(), Duration.ofHours(1));
        reservationRepository.save(reservation);

        Reservation savedReservation = reservationRepository.getOne(reservation.getId());

        assertNotNull(savedReservation);
    }
}
